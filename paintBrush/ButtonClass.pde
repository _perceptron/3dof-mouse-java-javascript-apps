
public class MyButton
{
    float x,y,width,height;
    String mButtonText;
    boolean on;
    boolean clicked = false;
    boolean buttonEnabled = false;
   
    MyButton ( float xx, float yy, float ww, float hh , String text) 
    {
        x = xx; y = yy; width = ww; height = hh; mButtonText = text;
        Interactive.add( this ); // add this to GUIDO manager, important!
    }

    void mousePressed ()
    {
        // called when the button has been pressed
        if(buttonEnabled == true)
        {
          background(255);
          clicked = true;
        }
    }
    
    void enable(){
      buttonEnabled = true;
    }
    
     void disable(){
      buttonEnabled = false;
    }

    void draw ()
    {
        // called by GUIDO after PApplet.draw() has finished
        
        if(buttonEnabled == true)
        {
        fill(180,180,180,100);
        //stroke(200);
        noStroke();
        rect( x, y, width, height/2, 7, 7, 0 ,0 );
        fill(130,130,130,100);
        rect( x, y + height/2 , width, height/2, 0 ,0 , 7, 7 );
        fill(255);
        textSize(20);
        text(mButtonText, x+5, y+0.75*height);
        }
        
    }
    
//    boolean isMouseOverButton()
//    {
//      
//    }
}
