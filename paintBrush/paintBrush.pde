import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

import de.bezier.guido.*;

/* @pjs preload="paintBrush.png"; */

/* *** References */
/* *** Bezier function. http://processingjs.org/reference/bezier_/ */
/* *** Used Bezier tool to create the curve graphical */
/* *** References */
/* *** References */

int x =0;
float rand_x, rand_y, rand_z;
boolean flag = true;
PImage img;
float ellipseSize = 30.0;
final int clearButtonX = 550;
final int clearButtonY = 20;
PFont mono;

AniSequence seq;

void setup() 
{
  
  size(640, 360,P3D);
  background(255);
  smooth();
  frameRate(50);
  img = loadImage("paintBrush.png");
  
  Interactive.make( this ); // start GUIDO
  MyButton mb = new MyButton( clearButtonX, clearButtonY, 60, 30, "Clear" ); // create an instance of your element
  mb.enable();

  // The font "AndaleMono-48.vlw"" must be located in the 
  // current s]ketch's "data" directory to load successfully
  mono = loadFont("AndaleMono.vlw");
}

void draw()
{
  drawTarget();
  
  if(isMouseOverClearButton()){
    cursor(ARROW);
  }
  
  else{
    cursor(img);
    if (mousePressed) {
     stroke(234,69,69,20);
     colorMode(RGB);
     fill(234,69,69,20);
//      fill(100);
      ellipse(mouseX, mouseY, ellipseSize, ellipseSize);  // Draw gray ellipse using CORNERS mode
    }
  }
  //  println(ellipseSize);

}



void drawTarget(){
  smooth();
  
  noFill(); 

  stroke(0);
  beginShape();
  vertex(38.0, 143.0);
  bezierVertex(38.0, 143.0, 38.0, 143.0, 38.0, 143.0);
  bezierVertex(38.0, 143.0, 106.0, 142.0, 111.0, 143.0);
  bezierVertex(116.0, 144.0, 144.0, 144.0, 189.0, 156.0);
  bezierVertex(234.0, 168.0, 240.0, 170.0, 267.0, 175.0);
  bezierVertex(294.0, 180.0, 336.0, 179.0, 346.0, 179.0);
  bezierVertex(356.0, 179.0, 428.0, 178.0, 459.0, 178.0);
  endShape();
  
  beginShape();
  vertex(41.0, 173.0);
  bezierVertex(41.0, 173.0, 41.0, 173.0, 41.0, 173.0);
  bezierVertex(41.0, 173.0, 111.0, 173.0, 111.0, 173.0);
  bezierVertex(111.0, 173.0, 138.0, 173.0, 162.0, 173.0);
  bezierVertex(186.0, 173.0, 246.0, 183.0, 263.0, 187.0);
  bezierVertex(280.0, 191.0, 351.0, 201.0, 364.0, 202.0);
  bezierVertex(377.0, 203.0, 448.0, 209.0, 457.0, 209.0);
  endShape();

}

void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  //rotateAngle = radians(e*6);
  //println(rotateAngle);
  
  float rotationLimitConstant = 60;
  
  println(ellipseSize);
  ellipseSize = ellipseSize + 3*e;//map(e,-30,30,-10,10); 
  println(e);
  println(ellipseSize);
  
  
  if(ellipseSize >= rotationLimitConstant){
      ellipseSize = rotationLimitConstant;
  }
  
  else if(ellipseSize <= 10){
    ellipseSize = 10;
  }
  
}


boolean isMouseOverClearButton(){
  if (((mouseX > clearButtonX) && mouseX < (clearButtonX+80)) && 
        ((mouseY > clearButtonY) && (mouseY < (clearButtonY+30)))) {
      return true;
      } 
   else {
      return false;
  }
}
