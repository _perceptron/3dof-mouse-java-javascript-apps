import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

import de.bezier.guido.*;

/* @pjs preload="paintBrush.png"; */

/* *** References */
/* *** Bezier function. http://processingjs.org/reference/bezier_/ */
/* *** Used Bezier tool to create the curve graphical */
/* *** References */
/* *** References */

int x =0;
float rand_x, rand_y, rand_z;
boolean flag = true;
PImage img;
float ellipseSize = 30.0;
final int clearButtonX = 550;
final int clearButtonY = 20;
PFont mono;

AniSequence seq;

void setup() 
{
  
  size(640, 360,P3D);
  background(255);
  smooth();
  frameRate(50);
  img = loadImage("paintBrush.png");
  
  Interactive.make( this ); // start GUIDO
  MyButton mb = new MyButton( clearButtonX, clearButtonY, 80, 30 ); // create an instance of your element

  // The font "AndaleMono-48.vlw"" must be located in the 
  // current s]ketch's "data" directory to load successfully
  mono = loadFont("AndaleMono.vlw");


}

void draw()
{
  drawTarget();
  
  if(isMouseOverClearButton()){
    cursor(ARROW);
  }
  
  else{
    cursor(img);
    if (mousePressed) {
     stroke(234,69,69,20);
     colorMode(RGB);
     fill(234,69,69,20);
//      fill(100);
      ellipse(mouseX, mouseY, ellipseSize, ellipseSize);  // Draw gray ellipse using CORNERS mode
    }
  }
  //  println(ellipseSize);

}



void drawTarget(){
  smooth();
  
  noFill(); 
  
//beginShape();
//vertex(73.0, 171.0);
//bezierVertex(69.0, 181.0, 64.0, 185.0, 73.0, 171.0);
//bezierVertex(82.0, 157.0, 101.0, 134.0, 128.0, 124.0);
//bezierVertex(155.0, 114.0, 179.0, 121.0, 199.0, 129.0);
//bezierVertex(219.0, 137.0, 214.0, 138.0, 267.0, 157.0);
//bezierVertex(320.0, 176.0, 353.0, 156.0, 403.0, 107.0);
//endShape();
//
//beginShape();
//vertex(102.0, 204.0);
//bezierVertex(92.0, 202.0, 87.0, 235.0, 102.0, 204.0);
//bezierVertex(117.0, 173.0, 153.0, 142.0, 194.0, 155.0);
//bezierVertex(235.0, 168.0, 252.0, 175.0, 283.0, 180.0);
//bezierVertex(314.0, 185.0, 361.0, 181.0, 421.0, 142.0);
//endShape();

stroke(0);
beginShape();
vertex(38.0, 143.0);
bezierVertex(38.0, 143.0, 38.0, 143.0, 38.0, 143.0);
bezierVertex(38.0, 143.0, 106.0, 142.0, 111.0, 143.0);
bezierVertex(116.0, 144.0, 144.0, 144.0, 189.0, 156.0);
bezierVertex(234.0, 168.0, 240.0, 170.0, 267.0, 175.0);
bezierVertex(294.0, 180.0, 336.0, 179.0, 346.0, 179.0);
bezierVertex(356.0, 179.0, 428.0, 178.0, 459.0, 178.0);
endShape();

beginShape();
vertex(41.0, 173.0);
bezierVertex(41.0, 173.0, 41.0, 173.0, 41.0, 173.0);
bezierVertex(41.0, 173.0, 111.0, 173.0, 111.0, 173.0);
bezierVertex(111.0, 173.0, 138.0, 173.0, 162.0, 173.0);
bezierVertex(186.0, 173.0, 246.0, 183.0, 263.0, 187.0);
bezierVertex(280.0, 191.0, 351.0, 201.0, 364.0, 202.0);
bezierVertex(377.0, 203.0, 448.0, 209.0, 457.0, 209.0);
endShape();

}

//void mousePressed() {
//
//  fill(100);  // Set fill to gray
//   ellipse(mouseX, mouseY, ellipseSize, ellipseSize);  // Draw gray ellipse using CORNERS mode
//  
//}

void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  //rotateAngle = radians(e*6);
  //println(rotateAngle);
  
  float rotationLimitConstant = 60;
  
  println(ellipseSize);
  ellipseSize = ellipseSize + 3*e;//map(e,-30,30,-10,10); 
  println(e);
  println(ellipseSize);
  
  
  if(ellipseSize >= rotationLimitConstant){
      ellipseSize = rotationLimitConstant;
  }
  
  else if(ellipseSize <= 10){
    ellipseSize = 10;
  }
  
}

public class MyButton
{
    float x,y,width,height;
    boolean on;

    MyButton ( float xx, float yy, float ww, float hh ) 
    {
        x = xx; y = yy; width = ww; height = hh;
        Interactive.add( this ); // add this to GUIDO manager, important!
    }

    void mousePressed ()
    {
        // called when the button has been pressed
    background(255);
    }

    void draw ()
    {
        // called by GUIDO after PApplet.draw() has finished

//        fill( on ? 80 : 140 );
        textSize(20);
//        textFont(mono,20);
        fill(0, 102, 153);
        text("Clear", 560, 40);
        noFill();
        stroke(0);
        rect( x, y, width, height );
    }
}

boolean isMouseOverClearButton(){
  if (((mouseX > clearButtonX) && mouseX < (clearButtonX+80)) && 
        ((mouseY > clearButtonY) && (mouseY < (clearButtonY+30)))) {
      return true;
      } 
   else {
      return false;
  }
}

