float bx;
float by;
int boxSize = 75;
boolean overBox = false;
boolean locked = false;
float xOffset = 0.0; 
float yOffset = 0.0; 

void setup() 
{
  size(640, 360);
  bx = width/2.0;
  by = height/2.0;
 // rectMode(RADIUS);  
}



void draw() 
{ 
  background(255);
  // Test if the cursor is over the box 
  if (mouseX > bx-boxSize && mouseX < bx+boxSize && 
        mouseY > by-boxSize && mouseY < by+boxSize) {
      overBox = true;  
      if(!locked) { 
        stroke(0); 
        fill(252,131,0,50);
      } 
  } else {
      stroke(153);
      fill(252,131,0,50);
      overBox = false;
  }
  
  
  // rect(bx, by, boxSize, boxSize);
   //rotate(cos(0));
   star(bx, by, 30, 70,5);
   
 // pushMatrix();
 // translate(width/2.0,height/2.0);//(width*0.5, height*0.5);
  //rotate(cos(120));
 // star(0, 0, 30, 70, 5); 
 // popMatrix();

  
  pushMatrix();
  translate(width*0.8, height*0.5);
  rotate(cos(60));
  stroke(153);
  fill(0,252,111,50);
  star(0, 0, 30, 70, 5); 
  popMatrix();
  

}

void mousePressed() {
  if(overBox) { 
      locked = true; 
      fill(255, 255, 255);
  } else {
      locked = false;
  }
  xOffset = mouseX-bx; 
  yOffset = mouseY-by; 

}

void mouseDragged() {
  if(locked) {
      bx = mouseX-xOffset; 
      by = mouseY-yOffset; 
  }
}

void mouseReleased() {
  locked = false;
}

void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  println(e);
}

void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
      float sx = x + cos(a) * radius2;
      float sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a+halfAngle) * radius1;
      sy = y + sin(a+halfAngle) * radius1;
      vertex(sx, sy);
  }
  endShape(CLOSE);
}

