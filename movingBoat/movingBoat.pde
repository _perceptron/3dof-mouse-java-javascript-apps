/* @pjs preload="river.png"; */
/* @pjs preload="up.png"; */
/* @pjs preload="down.png"; */
/* @pjs preload="left.png"; */
/* @pjs preload="right.png"; */
import de.bezier.guido.*;

int[] xvals;
int[] yvals;
int[] bvals;
float BOAT_X = 100;
float BOAT_Y = 300;
boolean overBoat = false;
boolean locked = false;
final int boxXSize = 80;
final int boxYSize = 30;
float xOffset = 0.0; 
float yOffset = 0.0; 
float rotateAngle = 0.0;
float delta = (PI/40.0);
Slider slider;
PImage bg;
float scrollConstant = 0.0;
final int rectOriginX = -30;
final int rectOriginY = -10;
final int rectSizeX = 60;
final int rectSizeY = 20;
final int[] triCoordinatesX = {30, 30, 50};
final int[] triCoordinatesY = {-10, 10, 0};

PImage rightArrow;
PImage leftArrow;
PImage up;
PImage down;

void setup() 
{
  size(640, 360);
  smooth();
  frameRate(30);
  drawBoat(BOAT_X,BOAT_Y);

  Interactive.make( this ); // start GUIDO
  slider = new Slider( 5, 5, width-10, 16 );
  bg = loadImage("river.png");
  rightArrow = loadImage("right.png");
  leftArrow = loadImage("left.png");
  up = loadImage("up.png");
  down = loadImage("down.png");
}

int arrayindex = 0;

void draw()
{
background(bg);


if (((mouseX > (BOAT_X + rectOriginX)) && mouseX < (BOAT_X+boxXSize + rectOriginX)) && 
        ((mouseY > BOAT_Y - boxYSize) && (mouseY < BOAT_Y+boxYSize))) {
      overBoat = true;  
      if(!locked) { 
        stroke(255); 
      fill(#1EF72E);
      } 
  } else {
      stroke(153);
      fill(#FFFFFF);
      overBoat = false;
  }
  
  drawBoat(BOAT_X,BOAT_Y);
}

void mousePressed() {
if(overBoat) { 
      locked = true; 
      fill(#1BBF27);
  } else {
      locked = false;
      fill(#1BBF27);
  }
  
  xOffset = mouseX-BOAT_X; 
  yOffset = mouseY-BOAT_Y; 
}

void mouseDragged() {
  if(locked){
      BOAT_X = mouseX - xOffset;
      BOAT_Y = mouseY - yOffset;
  }
}


void mouseReleased() {
  locked = false;
}


void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  //rotateAngle = radians(e*6);
  //println(rotateAngle);
  
  float rotationLimitConstant = 0.5;
  
  println(scrollConstant);
  scrollConstant = scrollConstant + map(e,-30,30,-rotationLimitConstant,rotationLimitConstant); 
  println(scrollConstant);
  
  
  if(scrollConstant >= rotationLimitConstant){
      scrollConstant = -rotationLimitConstant;
  }
  
  else if(scrollConstant <= -rotationLimitConstant){
    scrollConstant = rotationLimitConstant;
  }
  
  rotateAngle = -(2*PI*scrollConstant);

  
//  println(e);
//println(rotateAngle);
}
//
//void mouseScrolled()
//{
//   float rotationLimitConstant = 0.5;
////    println(scrollConstant);
//    scrollConstant = scrollConstant + map(mouseScroll,-30,30,-rotationLimitConstant,rotationLimitConstant); 
////    println(scrollConstant);
//    
//    
//    if(scrollConstant >= rotationLimitConstant){
//        scrollConstant = -rotationLimitConstant;
//    }
//    
//    else if(scrollConstant <= -rotationLimitConstant){
//      scrollConstant = rotationLimitConstant;
//    }
//    /*Uncomment the line below to incorporate effect of mouse scroll*/
////    rotateAngle = -(2*PI*scrollConstant);
//}


void drawBoat(float x, float y)
{
//  background(200);
  pushMatrix();
  translate(x, y);
  rotate(rotateAngle);
  
  /*Triangle and rectangle about zero*/
  //triangle(60, 0, 60, 20, 80, 10);
  //rect(0, 0, 60, 20);

  /*Triangle and rectangle about (30,-10)*/
  triangle(triCoordinatesX[0], triCoordinatesY[0], triCoordinatesX[1], triCoordinatesY[1], triCoordinatesX[2], triCoordinatesY[2]);
  rect(rectOriginX, rectOriginY, rectSizeX, rectSizeY);      
  popMatrix();
}

boolean isOnBoat(){
    
  return false;
}

void drawRiverBoundary1(){
  
  smooth();

  noFill();
//  fill(#79AFF0);
  stroke(0);
  beginShape();
  curveVertex(100, 160); // the first control point
  curveVertex(100, 160); // is also the start point of curve
  curveVertex(180, 120);
  curveVertex(280, 150);
  curveVertex(340, 100); 
  curveVertex(400, 110); 
  
  curveVertex(460, 160); // the last point of curve
  curveVertex(460, 160); // is also the last control point
  endShape();
}

void drawRiverBoundary2(){
  
  smooth();

  noFill();
//   fill(#79AFF0);
  stroke(0);
  beginShape();
  curveVertex(100, 220); // the first control point
  curveVertex(100, 220); // is also the start point of curve
  curveVertex(180, 180);
  curveVertex(280, 210);
  curveVertex(340, 160); 
  curveVertex(400, 170); 
  
  curveVertex(460, 220); // the last point of curve
  curveVertex(460, 220); // is also the last control point
  endShape();
}

public class MyButton
{
    float x,y,width,height;
    boolean on;
    String text;

    MyButton ( float xx, float yy, float ww, float hh, String buttonText ) 
    {
        x = xx; y = yy; width = ww; height = hh; text = buttonText;

        Interactive.add( this ); // add this to GUIDO manager, important!
    }

    void mousePressed ()
    {
        // called when the button has been pressed

//        on = !on;
      if(text == "Turn Right"){
        rotateAngle = rotateAngle + delta;
      }
      
      else if(text == "Turn Left"){
         rotateAngle = rotateAngle - delta;
      }

    }

    void draw ()
    {
        // called by GUIDO after PApplet.draw() has finished

//        fill( on ? 80 : 140 );

        if(text == "Turn Right"){
          fill(#20D62E);
        }
        else if(text == "Turn Left"){
          fill(#EA1C1C);
        }
        
        textSize(16);
        text(text, x, y-10); 
        rect( x, y, width, height );
       
    }
}

public class Slider
{
    float x, y, width, height;
    float valueX = 0;
    float value = 0;
    
    
    Slider ( float xx, float yy, float ww, float hh ) 
    {
        x = xx; 
        y = yy; 
        width = ww; 
        height = hh;
        
        valueX = width/2;
    
        // register it
        Interactive.add( this );
    }
    
    // called from manager
    void mouseDragged ( float mx, float my )
    {
        valueX = mx - height/2;
        
        if ( valueX < x ) valueX = x;
        if ( valueX > x+width-height ) valueX = x+width-height;
        
        value = map( valueX, x, x+width-height, 0, 1 );
        
        rotateAngle = (2*PI*value - PI);
//        println(value);
    }

    void draw () 
    {
        noStroke();
        
        fill( 100 );
        rect(x, y, width, height);
        
        fill( 200 );
        rect( valueX, y, height, height );
        
        textSize(18);
        fill(0, 102, 153);
        text("Turn Right", 450, 40); 
        fill(0, 102, 153);
        text("Turn Left", 100, 40); 
        text("Slider", 300, 40); 
        
        image(rightArrow,340,22,62,28);
        image(leftArrow,250,20,51,22);
        image(up,100,40,63,48);
        image(down,450,40,73,45);
    }
}
