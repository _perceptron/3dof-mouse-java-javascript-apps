import de.bezier.guido.*;
import seltar.motion.*;

/* @pjs preload="movableCar.png"; */
/* @pjs preload="carmod_small.png"; */
/* @pjs preload="arrow.png"; */
/* @pjs preload="rotatingCarsDemoTransparent.png"; */


import java.util.LinkedList;
import java.awt.event.KeyEvent;

PImage movableCarGreen;
PImage movableCarRed;
PImage movableCar;
PImage background;
PImage cursorMove;
PImage arrowImg;
PImage rotatingCarsDemo;

int canvasSizeX=640;
int canvasSizeY=480;
float incr = 0.025;
float a=PI/6.0; 
float wheel;
float bx;
float by;
int boxSize = 75;
boolean Pressed = false;
boolean overBox = false;
boolean locked = false;
boolean overCar = false; 
float xOffset = 0.0;
float yOffset = 0.0;
float movableCarX = bx;
float movableCarY = by;
float carSizeX = 340; //230
float carSizeY = 202; //130
int trial=0;
float angle = PI/6.0;
boolean stop=false;
boolean move=false;
boolean done=false;
boolean first=false;
float an;

float arrowSizeX = 190;
float arrowSizeY= 70;

float angle_arrow;

float theta_arrow[] = {
  -PI, -3*PI/4, -PI/2, -PI/4, 0, PI/4, PI/2, 3*PI/4
};
float theta_angle[] = {
  -PI/2, -PI/3, -PI/4, -PI/6, PI/6, PI/4, PI/3, PI/2
};

LinkedList<trialAngle> myAngleList = new LinkedList();
int myAngleListIndex;

boolean sessionIsPractice = true;
int tryAgainButtonX = 260;
int tryAgainButtonY = 172;
int tryAgainButtonWidth = 100;
int tryAgainButtonHeight = 30;
MyButton buttonTryAgain;

int startButtonX = 218;
int startButtonY = 223;
int startButtonWidth = 203;
int startButtonHeight = 22;
MyButton buttonStart;

int introButtonX = 550;
int introButtonY = 430;
int introButtonWidth = 60;
int introButtonHeight = 30;
MyButton buttonIntroNext;

boolean firstActualTrial = true;
boolean introState = true;

void setup()
{
  size(canvasSizeX, canvasSizeY);
  //size(displayWidth, displayHeight);
  bx = width/2.0;
  by = height/2.0;
  movableCar = loadImage("movableCar.png");
  background = loadImage("carmod_small.png");
  arrowImg = loadImage("arrow.png");
  rotatingCarsDemo = loadImage("rotatingCarsDemoTransparent.png");
  movableCarRed = loadImage("movableCarRed.png");
  movableCarGreen = loadImage("movableCarGreen.png");

  for (int i=0;i<theta_arrow.length;i++)
  {
    for (int j=0;j<theta_angle.length;j++)
    {
      trialAngle indexedTrialAngle = new trialAngle(theta_arrow[i], theta_angle[j]);
      myAngleList.add(indexedTrialAngle);
    }
  }
  mouseLoggerInit("RigorousStudyUserData.csv");
  
  sessionIsPractice = true;
  
  Interactive.make( this ); // start GUIDO
  buttonTryAgain = new MyButton( tryAgainButtonX, tryAgainButtonY, tryAgainButtonWidth, tryAgainButtonHeight, "Try Again" ); // create an instance of your element
  buttonStart = new MyButton( startButtonX, startButtonY, startButtonWidth, startButtonHeight, "Ok got it. Let's Start!" ); // create an instance of your element
  buttonIntroNext = new MyButton(introButtonX,introButtonY,introButtonWidth,introButtonHeight,"Next");

}


void draw() 
{
  background(242, 241, 236);
//  fill(0);
//  textSize(15);
//  text("MouseX:" + mouseX,width -100,30);
//  text("MouseY:" + mouseY,width -100,60);
  
  if(introState == true)
  {
      fill(0);
      textSize(16);
      text("In this task, your goal is to click on the Red Car, rotate the car by turning the \nmouse until you match it against the Green Car.", 20, 40);
      image(rotatingCarsDemo, 100, 100, 440, 342);
      buttonIntroNext.enable();
      
      if(isMouseOverIntroNextButton())
      {
        cursor(HAND);
        
        if(Pressed)
        {
          introState = false;
          buttonIntroNext.disable();
        }
      }
      
      else
      {
       cursor(ARROW); 
      }
     
//     textSize(20);
//     text("Next", 550,430);

  }
  
  else
  {
  
      if(sessionIsPractice == true)
      {
    //     fill(0);
    //     textSize(20);
    //     text("Click to start Task", width/2-80, height/2);
         
         rotationTaskPractise();
      }
      
      else if(sessionIsPractice == false)
      {
       
       if(firstActualTrial)
       { 
       resetVariables();
       buttonStart.disable();
       buttonTryAgain.disable(); 
       firstActualTrial = false;
       } 
       
       rotationTask();
       //rotationTaskPractise();
      }
      
  }
  
  
}//void draw()

void rotationTaskPractise()
{
    if (done == true)
    {
      pushMatrix();
      //done=false;
      translate(bx, by);
       
       buttonTryAgain.enable();
       buttonStart.enable();
   
      if (isMouseOverTryAgainButton()==true)
      {
        cursor(HAND);
        if (Pressed == true)
        {
          trial = trial+1;
  
          a = myAngleList.get(myAngleListIndex).mThetaAngle;
          println(myAngleList.get(myAngleListIndex).mThetaAngle);
          println(myAngleList.get(myAngleListIndex).mThetaArrow);
          myAngleList.remove(myAngleListIndex);
          angle = a;
          done = false;
  
          if (myAngleList.size() == 0)
          {
            stop=true;
          }
          Pressed = false;
  
          cursor(ARROW);
        }
      }//if (isCursorOverNext()==true) ends  here
    
    else if(isMouseOverStartButton() == true)
      {
         cursor(HAND); 
         if(Pressed)
         {
           sessionIsPractice = false; 
         }
      }  
      
    else if (isMouseOverTryAgainButton()==false && isMouseOverStartButton() == false)
      {
        cursor(ARROW);
      }
      popMatrix();
      
    }//if(done == true)
  
    if (done == false) 
    {
      buttonStart.disable();
      buttonTryAgain.disable();
      
      if (move==true)
      {
        pushMatrix();
        translate(bx, by);
        fill(0, 0, 0);
        textSize(20);
        text("Good Job!!!", bx-630, by-450);
        fill(0, 0, 0);
        textSize(17);
        text("Next, move the car along the direction pointed by the arrow.", bx-510, by-450);
        rotate(-angle);
        rotate(a);
        rotate(angle_arrow);
        image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
        popMatrix();
        pushMatrix();// click and drag the car
        //translate(movableCarX-2, movableCarY+81);
  
        //translate(mouseX-(carSizeX/2.0),mouseY+(carSizeY/2.0));
        translate(movableCarX, movableCarY);
        //translate(bx, by);
        rotate(-angle);
        rotate(a);
        image(movableCarRed, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
        if (((mouseX-bx)*(mouseX-bx))+((mouseY-by)*(mouseY-by)) > 51057)
        {
          if (isCursorOverCar() == true)
          {
            move = false;
            done = true;
            movableCarX = bx;
            movableCarY = by;
            xOffset =0.0; 
            yOffset =0.0;
          }
        }
  
        popMatrix();
      }//if(move==true)
  
  
      if (move==false)
      {
        movableCarX = bx;
        movableCarY = by;
        if (stop==true)
        {
          background(177, 252, 140);
          fill(0, 0, 0);
          textSize(50);
          text("You did a great job!", bx-230, by-50);
          textSize(70);
          text("Thank You!!!", bx-200, by+50);
        }
        else if (stop==false) 
        {
          fill(0);
          textSize(16);
          text("Place the cursor on the Red Car, rotate the car by turning the \nmouse until you match it against the Green Car.", 20, 40);
          
          pushMatrix();
          translate(bx, by);
          rotate(-angle);
          // tint(0, 153, 204, 255); 
          image(movableCarGreen, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
          overCar = isCursorOverCar();
          if (isCursorOverCar() == true)
            cursor(CROSS);
          else
            cursor(ARROW);
          if (wheel == 0)
            a = a;
          if (wheel>0)
          {
            a = a+incr;
            //wheel = 0;
          }
          else if (wheel<0)
          {
            a = a-incr;
            //wheel = 0;
          }
          rotate(a);
          noTint();
          image(movableCarRed, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
          //println("a=",a);
          wheel = 0;
          popMatrix();
          if ((a==0)||((a>-.02)&&(a<0.02))||((a<-6.26)&&(a>-6.29)) || ((a>6.24)&&(a<6.28)))
          {
            a=0;
            pushMatrix();
            translate(bx, by);
            rotate(-angle);
            rotate(a);
            pushMatrix();
            myAngleListIndex = (int)random(myAngleList.size());            
            angle_arrow = myAngleList.get(myAngleListIndex).mThetaArrow;
            rotate(angle_arrow);
            image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
            popMatrix();
            rotate(angle);
//            fill(0, 0, 0);
//            textSize(20);
//            text("Please move the car along the direction pointed by the arrow", bx-600, by-350);
//            fill(0, 0, 0);
//            textSize(90);
//            text("Good Job!!!", bx-560, by-400);               
            popMatrix();
            if (isCursorOverCar()==true)
            {
              cursor(HAND);
              move=true;
              first=true;
            }
            else if (isCursorOverCar()==false)
            {
              cursor(ARROW);
            }
  
            //popMatrix();
          }//if ((a==0)||((a>-.02)&&(a<0.02))||((a>6.24)&&(a>6.28))) ends here
          //popMatrix();
        }//else if(stop==false)
      }//if(move==false)
    }//if(done==false)  
}

void rotationTask()
{
  if (done == true)
    {
      pushMatrix();
      //done=false;
      translate(bx, by);
      fill(0, 0, 0);
      trial = trial+1;

      a = myAngleList.get(myAngleListIndex).mThetaAngle;
      println(myAngleList.get(myAngleListIndex).mThetaAngle);
      println(myAngleList.get(myAngleListIndex).mThetaArrow);
      myAngleList.remove(myAngleListIndex);
      angle = a;
      done = false;
      if (myAngleList.size() == 0)
      {
        stop=true;
      }

      popMatrix();
    }//if(done == true)
  
    if (done == false) 
    {
      if (move==true)
      {
        pushMatrix();
        translate(bx, by);
        fill(0, 0, 0);
        rotate(-angle);
        rotate(a);
        rotate(angle_arrow);
        image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
        popMatrix();
        pushMatrix();// click and drag the car
        //translate(movableCarX-2, movableCarY+81);
  
        //translate(mouseX-(carSizeX/2.0),mouseY+(carSizeY/2.0));
        translate(movableCarX, movableCarY);
        //translate(bx, by);
        rotate(-angle);
        rotate(a);
        image(movableCarRed, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
        if (((mouseX-bx)*(mouseX-bx))+((mouseY-by)*(mouseY-by)) > 51057)
        {
          if (isCursorOverCar() == true)
          {
            move = false;
            done = true;
            movableCarX = bx;
            movableCarY = by;
            xOffset =0.0; 
            yOffset =0.0;
          }
        }
  
        popMatrix();
      }//if(move==true)
  
  
      if (move==false)
      {
        movableCarX = bx;
        movableCarY = by;
        if (stop==true)
        {
          background(177, 252, 140);
          fill(0, 0, 0);
          textSize(50);
          text("You did a great job!", bx-230, by-50);
          textSize(70);
          text("Thank You!!!", bx-200, by+50);
        }
        else if (stop==false)
        {
          pushMatrix();
          translate(bx, by);
          rotate(-angle);
          // tint(0, 153, 204, 255); 
          image(movableCarGreen, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
          overCar = isCursorOverCar();
          if (isCursorOverCar() == true)
            cursor(CROSS);
          else
            cursor(ARROW);
          if (wheel == 0)
            a = a;
          if (wheel>0)
          {
            a = a+incr;
            //wheel = 0;
          }
          else if (wheel<0)
          {
            a = a-incr;
            //wheel = 0;
          }
          rotate(a);
          noTint();
          image(movableCarRed, -(carSizeX/2.0), -(carSizeY/2.0), carSizeX, carSizeY);
          //println("a=",a);
          wheel = 0;
          popMatrix();
          if ((a==0)||((a>-.02)&&(a<0.02))||((a<-6.26)&&(a>-6.29)) || ((a>6.24)&&(a<6.28)))
          {
            a=0;
            pushMatrix();
            translate(bx, by);
            rotate(-angle);
            rotate(a);
            pushMatrix();
            myAngleListIndex = (int)random(myAngleList.size());            
            angle_arrow = myAngleList.get(myAngleListIndex).mThetaArrow;
            rotate(angle_arrow);
            image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
            popMatrix();
            rotate(angle);
                  
            popMatrix();
            if (isCursorOverCar()==true)
            {
              cursor(HAND);
              move=true;
              first=true;
            }
            else if (isCursorOverCar()==false)
            {
              cursor(ARROW);
            }
  
            //popMatrix();
          }//if ((a==0)||((a>-.02)&&(a<0.02))||((a>6.24)&&(a>6.28))) ends here
          //popMatrix();
        }//else if(stop==false)
      }//if(move==false)
    }//if(done==false)  
}

void resetVariables()
{
  Pressed = false;
  overBox = false;
  locked = false;
  overCar = false; 
  trial=0;
  angle = PI/6.0;
  a = angle;
  stop=false;
  move=false;
  done=false;
  first=false;
}

void mouseWheel(MouseEvent event) {
  //if (locked == true)
  if (isCursorOverCar()==true)
    wheel = event.getAmount();
    
    mouseLog(MOUSE_WHEEL);
    
}

boolean isCursorOverCar() {
  if (mouseX > movableCarX-130 && mouseX < movableCarX+carSizeX+20-130 &&
    mouseY > movableCarY-80 && mouseY < movableCarY+carSizeY+50-80) 
  {
    return true;
  }
  return false;
}

boolean isCursorOverNext() {
  
  //x = 576,639    y = 440,464
  if ((mouseX>(260)) && (mouseX<(350)) && mouseY>172 && mouseY<200 )
    {
    return true;
    }
  else
  {
    return false;
  }
}


boolean isMouseOverTryAgainButton(){
  if (((mouseX > tryAgainButtonX) && mouseX < (tryAgainButtonX+tryAgainButtonWidth)) && 
        ((mouseY > tryAgainButtonY) && (mouseY < (tryAgainButtonY+tryAgainButtonHeight)))) {
      return true;
      } 
   else {
      return false;
  }
}

boolean isMouseOverStartButton(){
  if (((mouseX > startButtonX) && mouseX < (startButtonX+startButtonWidth)) && 
        ((mouseY > startButtonY) && (mouseY < (startButtonY+startButtonHeight)))) {
      return true;
      } 
   else {
      return false;
  }
}


boolean isMouseOverIntroNextButton(){
  if (((mouseX > introButtonX) && mouseX < (introButtonX + introButtonWidth)) && 
        ((mouseY > introButtonY) && (mouseY < (introButtonY+ introButtonHeight)))) {
      return true;
      } 
   else {
      return false;
  }
}

void mouseReleased() {
  Pressed=false;
  locked = false;
  cursor(ARROW);

  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+ MOUSE_RELEASED);
  output.flush();
  
  mouseLog(MOUSE_RELEASED);
}


void mousePressed() {
  Pressed = true;
  if (isCursorOverCar()) { 
    locked = true; 
    //cursor(HAND);
  } 
  else {
    locked = false;
  }
  xOffset = mouseX-movableCarX; 
  yOffset = mouseY-movableCarY; 

  //String currentHour = new String(hour());
  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+MOUSE_PRESSED);
  output.flush();
 
  mouseLog(MOUSE_PRESSED);
}

void mouseDragged() {
  if (locked) {
    movableCarX = mouseX-xOffset; 
    movableCarY = mouseY-yOffset;
  }
  
  mouseLog(MOUSE_PRESSED);
}


void keyPressed() {
  
  if(key == CODED)
  {
     if(keyCode == KeyEvent.VK_ESCAPE)
     {
       output.flush(); // Writes the remaining data to the file
       output.close(); // Finishes the file
       exit(); // Stops the program
     }
  }
  
}
