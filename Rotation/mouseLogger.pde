import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

static final String MOUSE_PRESSED = "MOUSE_PRESSED";
static final String MOUSE_RELEASED = "MOUSE_RELEASED";
static final String MOUSE_DRAGGED = "MOUSE_DRAGGED";
static final String MOUSE_WHEEL = "MOUSE_WHEEL";
PrintWriter output;

void mouseLog (String event)
{
   String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+event);
  output.flush();
}

void mouseLoggerInit(String fileName)
{
  output = createWriter(fileName);
}
