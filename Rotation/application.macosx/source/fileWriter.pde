import java.io.FileWriter;
import java.io.IOException;

public class fileWriterClass{
  
  String mFileName;
  FileWriter mWriter;
  
  private fileWriterClass(String fileName)
  {
     createCsvFile(fileName); 
  }
  
  
  private void createCsvFile(String fileName)
  {
     mFileName = fileName;
     
    
     try
      {
        mWriter = new FileWriter(mFileName);
        mWriter.close();
        println("File Created");
      }
    
     catch(IOException e)
     {
       e.printStackTrace();
       println("IOException");
     }  
  }
  
  private void writeEventToFile(String time, String Event){
      
    try
    {
      mWriter.write("Start" );
      mWriter.append(Event);
      mWriter.append(',');
      mWriter.append(time);
      mWriter.append('\n');
      mWriter.flush();
      mWriter.close();
    }
    
    catch(IOException e)
    {
      
    }
  }
  
}
