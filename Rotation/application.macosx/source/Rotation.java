import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import de.bezier.guido.*; 
import seltar.motion.*; 
import java.util.LinkedList; 
import java.awt.event.KeyEvent; 
import java.io.FileWriter; 
import java.io.IOException; 
import java.util.Date; 
import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.util.Calendar; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Rotation extends PApplet {




/* @pjs preload="movableCar.png"; */
/* @pjs preload="carmod_small.png"; */
/* @pjs preload="arrow.png"; */




PImage movableCarGreen;
PImage movableCarRed;
PImage movableCar;
PImage background;
PImage cursorMove;
PImage arrowImg;

int canvasSizeX=640;
int canvasSizeY=480;
float incr = 0.025f;
float a=PI/6.0f; 
float wheel;
float bx;
float by;
int boxSize = 75;
boolean Pressed = false;
boolean overBox = false;
boolean locked = false;
boolean overCar = false; 
float xOffset = 0.0f;
float yOffset = 0.0f;
float movableCarX = bx;
float movableCarY = by;
float carSizeX = 230;
float carSizeY = 130;
int trial=0;
float angle = PI/6.0f;
boolean stop=false;
boolean move=false;
boolean done=false;
boolean first=false;
float an;

float arrowSizeX = 190;
float arrowSizeY= 70;

float angle_arrow;

float theta_arrow[] = {
  -PI, -3*PI/4, -PI/2, -PI/4, 0, PI/4, PI/2, 3*PI/4
};
float theta_angle[] = {
  -PI/2, -PI/3, -PI/4, -PI/6, PI/6, PI/4, PI/3, PI/2
};

LinkedList<trialAngle> myAngleList = new LinkedList();
int myAngleListIndex;

boolean sessionIsPractice = true;
int tryAgainButtonX = 260;
int tryAgainButtonY = 172;
int tryAgainButtonWidth = 100;
int tryAgainButtonHeight = 30;
MyButton buttonTryAgain;

int startButtonX = 218;
int startButtonY = 223;
int startButtonWidth = 203;
int startButtonHeight = 22;
MyButton buttonStart;

boolean firstActualTrial = true;

public void setup()
{
  size(canvasSizeX, canvasSizeY);
  //size(displayWidth, displayHeight);
  bx = width/2.0f;
  by = height/2.0f;
  movableCar = loadImage("movableCar.png");
  background = loadImage("carmod_small.png");
  arrowImg = loadImage("arrow.png");

  for (int i=0;i<theta_arrow.length;i++)
  {
    for (int j=0;j<theta_angle.length;j++)
    {
      trialAngle indexedTrialAngle = new trialAngle(theta_arrow[i], theta_angle[j]);
      myAngleList.add(indexedTrialAngle);
    }
  }
  mouseLoggerInit("RigorousStudyUserData.csv");
  
  sessionIsPractice = true;
  
  Interactive.make( this ); // start GUIDO
  buttonTryAgain = new MyButton( tryAgainButtonX, tryAgainButtonY, tryAgainButtonWidth, tryAgainButtonHeight, "Try Again" ); // create an instance of your element
  buttonStart = new MyButton( startButtonX, startButtonY, startButtonWidth, startButtonHeight, "Ok got it. Let's Start!" ); // create an instance of your element

}


public void draw() 
{
  background(242, 241, 236);
  textSize(15);
  text("MouseX:" + mouseX,width -100,30);
  text("MouseY:" + mouseY,width -100,60);
  
  if(sessionIsPractice == true)
  {
//     fill(0);
//     textSize(20);
//     text("Click to start Task", width/2-80, height/2);
     
     rotationTaskPractise();
  }
  
  else if(sessionIsPractice == false)
  {
   
   if(firstActualTrial)
   { 
   resetVariables();
   buttonStart.disable();
   buttonTryAgain.disable(); 
   firstActualTrial = false;
   } 
   
   rotationTask();
   //rotationTaskPractise();
  }
}//void draw()

public void rotationTaskPractise()
{
    if (done == true)
    {
      pushMatrix();
      //done=false;
      translate(bx, by);
       
       buttonTryAgain.enable();
       buttonStart.enable();
   
      if (isMouseOverTryAgainButton()==true)
      {
        cursor(HAND);
        if (Pressed == true)
        {
          trial = trial+1;
  
          a = myAngleList.get(myAngleListIndex).mThetaAngle;
          println(myAngleList.get(myAngleListIndex).mThetaAngle);
          println(myAngleList.get(myAngleListIndex).mThetaArrow);
          myAngleList.remove(myAngleListIndex);
          angle = a;
          done = false;
  
          if (myAngleList.size() == 0)
          {
            stop=true;
          }
          Pressed = false;
  
          cursor(ARROW);
        }
      }//if (isCursorOverNext()==true) ends  here
    
    else if(isMouseOverStartButton() == true)
      {
         cursor(HAND); 
         if(Pressed)
         {
           sessionIsPractice = false; 
         }
      }  
      
    else if (isMouseOverTryAgainButton()==false && isMouseOverStartButton() == false)
      {
        cursor(ARROW);
      }
      popMatrix();
      
    }//if(done == true)
  
    if (done == false) 
    {
      buttonStart.disable();
      buttonTryAgain.disable();
      
      if (move==true)
      {
        pushMatrix();
        translate(bx, by);
        fill(0, 0, 0);
        textSize(90);
        text("Good Job!!!", bx-560, by-400);
        fill(0, 0, 0);
        textSize(20);
        text("Please move the car along the direction pointed by the arrow", bx-600, by-350);
        rotate(-angle);
        rotate(a);
        rotate(angle_arrow);
        image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
        popMatrix();
        pushMatrix();// click and drag the car
        //translate(movableCarX-2, movableCarY+81);
  
        //translate(mouseX-(carSizeX/2.0),mouseY+(carSizeY/2.0));
        translate(movableCarX, movableCarY);
        //translate(bx, by);
        rotate(-angle);
        rotate(a);
        image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
        if (((mouseX-bx)*(mouseX-bx))+((mouseY-by)*(mouseY-by)) > 51057)
        {
          if (isCursorOverCar() == true)
          {
            move = false;
            done = true;
            movableCarX = bx;
            movableCarY = by;
            xOffset =0.0f; 
            yOffset =0.0f;
          }
        }
  
        popMatrix();
      }//if(move==true)
  
  
      if (move==false)
      {
        movableCarX = bx;
        movableCarY = by;
        if (stop==true)
        {
          background(177, 252, 140);
          fill(0, 0, 0);
          textSize(50);
          text("You did a great job!", bx-230, by-50);
          textSize(70);
          text("Thank You!!!", bx-200, by+50);
        }
        else if (stop==false)
        {
          pushMatrix();
          translate(bx, by);
          rotate(-angle);
          // tint(0, 153, 204, 255); 
          image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
          overCar = isCursorOverCar();
          if (isCursorOverCar() == true)
            cursor(CROSS);
          else
            cursor(ARROW);
          if (wheel == 0)
            a = a;
          if (wheel>0)
          {
            a = a+incr;
            //wheel = 0;
          }
          else if (wheel<0)
          {
            a = a-incr;
            //wheel = 0;
          }
          rotate(a);
          noTint();
          image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
          //println("a=",a);
          wheel = 0;
          popMatrix();
          if ((a==0)||((a>-.02f)&&(a<0.02f))||((a<-6.26f)&&(a>-6.29f)) || ((a>6.24f)&&(a<6.28f)))
          {
            a=0;
            pushMatrix();
            translate(bx, by);
            rotate(-angle);
            rotate(a);
            pushMatrix();
            myAngleListIndex = (int)random(myAngleList.size());            
            angle_arrow = myAngleList.get(myAngleListIndex).mThetaArrow;
            rotate(angle_arrow);
            image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
            popMatrix();
            rotate(angle);
            fill(0, 0, 0);
            textSize(20);
            text("Please move the car along the direction pointed by the arrow", bx-600, by-350);
            fill(0, 0, 0);
            textSize(90);
            text("Good Job!!!", bx-560, by-400);               
            popMatrix();
            if (isCursorOverCar()==true)
            {
              cursor(HAND);
              move=true;
              first=true;
            }
            else if (isCursorOverCar()==false)
            {
              cursor(ARROW);
            }
  
            //popMatrix();
          }//if ((a==0)||((a>-.02)&&(a<0.02))||((a>6.24)&&(a>6.28))) ends here
          //popMatrix();
        }//else if(stop==false)
      }//if(move==false)
    }//if(done==false)  
}

public void rotationTask()
{
  if (done == true)
    {
      pushMatrix();
      //done=false;
      translate(bx, by);
      fill(0, 0, 0);
      if (isCursorOverNext()==true)
      {
        cursor(HAND);
        if (Pressed == true)
        {
          trial = trial+1;
  
          a = myAngleList.get(myAngleListIndex).mThetaAngle;
          println(myAngleList.get(myAngleListIndex).mThetaAngle);
          println(myAngleList.get(myAngleListIndex).mThetaArrow);
          myAngleList.remove(myAngleListIndex);
          angle = a;
          done = false;
  
          if (myAngleList.size() == 0)
          {
            stop=true;
          }
          Pressed = false;
  
          cursor(ARROW);
        }
      }//if (isCursorOverNext()==true) ends  here
      popMatrix();
    }//if(done == true)
  
    if (done == false) 
    {
      if (move==true)
      {
        pushMatrix();
        translate(bx, by);
        fill(0, 0, 0);
        textSize(90);
        text("Good Job!!!", bx-560, by-400);
        fill(0, 0, 0);
        textSize(20);
        text("Please move the car along the direction pointed by the arrow", bx-600, by-350);
        rotate(-angle);
        rotate(a);
        rotate(angle_arrow);
        image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
        popMatrix();
        pushMatrix();// click and drag the car
        //translate(movableCarX-2, movableCarY+81);
  
        //translate(mouseX-(carSizeX/2.0),mouseY+(carSizeY/2.0));
        translate(movableCarX, movableCarY);
        //translate(bx, by);
        rotate(-angle);
        rotate(a);
        image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
        if (((mouseX-bx)*(mouseX-bx))+((mouseY-by)*(mouseY-by)) > 51057)
        {
          if (isCursorOverCar() == true)
          {
            move = false;
            done = true;
            movableCarX = bx;
            movableCarY = by;
            xOffset =0.0f; 
            yOffset =0.0f;
          }
        }
  
        popMatrix();
      }//if(move==true)
  
  
      if (move==false)
      {
        movableCarX = bx;
        movableCarY = by;
        if (stop==true)
        {
          background(177, 252, 140);
          fill(0, 0, 0);
          textSize(50);
          text("You did a great job!", bx-230, by-50);
          textSize(70);
          text("Thank You!!!", bx-200, by+50);
        }
        else if (stop==false)
        {
          pushMatrix();
          translate(bx, by);
          rotate(-angle);
          // tint(0, 153, 204, 255); 
          image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
          overCar = isCursorOverCar();
          if (isCursorOverCar() == true)
            cursor(CROSS);
          else
            cursor(ARROW);
          if (wheel == 0)
            a = a;
          if (wheel>0)
          {
            a = a+incr;
            //wheel = 0;
          }
          else if (wheel<0)
          {
            a = a-incr;
            //wheel = 0;
          }
          rotate(a);
          noTint();
          image(movableCar, -(carSizeX/2.0f), -(carSizeY/2.0f), carSizeX, carSizeY);
          //println("a=",a);
          wheel = 0;
          popMatrix();
          if ((a==0)||((a>-.02f)&&(a<0.02f))||((a<-6.26f)&&(a>-6.29f)) || ((a>6.24f)&&(a<6.28f)))
          {
            a=0;
            pushMatrix();
            translate(bx, by);
            rotate(-angle);
            rotate(a);
            pushMatrix();
            myAngleListIndex = (int)random(myAngleList.size());            
            angle_arrow = myAngleList.get(myAngleListIndex).mThetaArrow;
            rotate(angle_arrow);
            image(arrowImg, -arrowSizeX/2+150, -arrowSizeY/2, arrowSizeX, arrowSizeY);
            popMatrix();
            rotate(angle);
            fill(0, 0, 0);
            textSize(20);
            text("Please move the car along the direction pointed by the arrow", bx-600, by-350);
            fill(0, 0, 0);
            textSize(90);
            text("Good Job!!!", bx-560, by-400);               
            popMatrix();
            if (isCursorOverCar()==true)
            {
              cursor(HAND);
              move=true;
              first=true;
            }
            else if (isCursorOverCar()==false)
            {
              cursor(ARROW);
            }
  
            //popMatrix();
          }//if ((a==0)||((a>-.02)&&(a<0.02))||((a>6.24)&&(a>6.28))) ends here
          //popMatrix();
        }//else if(stop==false)
      }//if(move==false)
    }//if(done==false)  
}

public void resetVariables()
{
  Pressed = false;
  overBox = false;
  locked = false;
  overCar = false; 
  trial=0;
  angle = PI/6.0f;
  a = angle;
  stop=false;
  move=false;
  done=false;
  first=false;
}

public void mouseWheel(MouseEvent event) {
  //if (locked == true)
  if (isCursorOverCar()==true)
    wheel = event.getAmount();
    
    mouseLog(MOUSE_WHEEL);
    
}

public boolean isCursorOverCar() {
  if (mouseX > movableCarX-130 && mouseX < movableCarX+carSizeX+20-130 &&
    mouseY > movableCarY-80 && mouseY < movableCarY+carSizeY+50-80) 
  {
    return true;
  }
  return false;
}

public boolean isCursorOverNext() {
  
  //x = 576,639    y = 440,464
  if ((mouseX>(260)) && (mouseX<(350)) && mouseY>172 && mouseY<200 )
    {
    return true;
    }
  else
  {
    return false;
  }
}


public boolean isMouseOverTryAgainButton(){
  if (((mouseX > tryAgainButtonX) && mouseX < (tryAgainButtonX+tryAgainButtonWidth)) && 
        ((mouseY > tryAgainButtonY) && (mouseY < (tryAgainButtonY+tryAgainButtonHeight)))) {
      return true;
      } 
   else {
      return false;
  }
}

public boolean isMouseOverStartButton(){
  if (((mouseX > startButtonX) && mouseX < (startButtonX+startButtonWidth)) && 
        ((mouseY > startButtonY) && (mouseY < (startButtonY+startButtonHeight)))) {
      return true;
      } 
   else {
      return false;
  }
}

public void mouseReleased() {
  Pressed=false;
  locked = false;
  cursor(ARROW);

  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+ MOUSE_RELEASED);
  output.flush();
  
  mouseLog(MOUSE_RELEASED);
}


public void mousePressed() {
  Pressed = true;
  if (isCursorOverCar()) { 
    locked = true; 
    //cursor(HAND);
  } 
  else {
    locked = false;
  }
  xOffset = mouseX-movableCarX; 
  yOffset = mouseY-movableCarY; 

  //String currentHour = new String(hour());
  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+MOUSE_PRESSED);
  output.flush();
 
  mouseLog(MOUSE_PRESSED);
}

public void mouseDragged() {
  if (locked) {
    movableCarX = mouseX-xOffset; 
    movableCarY = mouseY-yOffset;
  }
  
  mouseLog(MOUSE_PRESSED);
}


public void keyPressed() {
  
  if(key == CODED)
  {
     if(keyCode == KeyEvent.VK_ESCAPE)
     {
       output.flush(); // Writes the remaining data to the file
       output.close(); // Finishes the file
       exit(); // Stops the program
     }
  }
  
}

public class MyButton
{
    float x,y,width,height;
    String mButtonText;
    boolean on;
    boolean clicked = false;
    boolean buttonEnabled = false;
   
    MyButton ( float xx, float yy, float ww, float hh , String text) 
    {
        x = xx; y = yy; width = ww; height = hh; mButtonText = text;
        Interactive.add( this ); // add this to GUIDO manager, important!
    }

    public void mousePressed ()
    {
        // called when the button has been pressed
        if(buttonEnabled == true)
        {
          background(255);
          clicked = true;
        }
    }
    
    public void enable(){
      buttonEnabled = true;
    }
    
     public void disable(){
      buttonEnabled = false;
    }

    public void draw ()
    {
        // called by GUIDO after PApplet.draw() has finished
        
        if(buttonEnabled == true)
        {
        fill(180,180,180,100);
        //stroke(200);
        noStroke();
        rect( x, y, width, height/2, 7, 7, 0 ,0 );
        fill(130,130,130,100);
        rect( x, y + height/2 , width, height/2, 0 ,0 , 7, 7 );
        fill(255);
        textSize(20);
        text(mButtonText, x+5, y+0.75f*height);
        }
        
    }
}



public class fileWriterClass{
  
  String mFileName;
  FileWriter mWriter;
  
  private fileWriterClass(String fileName)
  {
     createCsvFile(fileName); 
  }
  
  
  private void createCsvFile(String fileName)
  {
     mFileName = fileName;
     
    
     try
      {
        mWriter = new FileWriter(mFileName);
        mWriter.close();
        println("File Created");
      }
    
     catch(IOException e)
     {
       e.printStackTrace();
       println("IOException");
     }  
  }
  
  private void writeEventToFile(String time, String Event){
      
    try
    {
      mWriter.write("Start" );
      mWriter.append(Event);
      mWriter.append(',');
      mWriter.append(time);
      mWriter.append('\n');
      mWriter.flush();
      mWriter.close();
    }
    
    catch(IOException e)
    {
      
    }
  }
  
}





static final String MOUSE_PRESSED = "MOUSE_PRESSED";
static final String MOUSE_RELEASED = "MOUSE_RELEASED";
static final String MOUSE_DRAGGED = "MOUSE_DRAGGED";
static final String MOUSE_WHEEL = "MOUSE_WHEEL";
PrintWriter output;

public void mouseLog (String event)
{
   String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
  //myFileWriter.writeEventToFile(timeStamp,MOUSE_PRESSED);
  output.println(timeStamp+","+event);
  output.flush();
}

public void mouseLoggerInit(String fileName)
{
  output = createWriter(fileName);
}
class trialAngle{
 float mThetaArrow ;
 float mThetaAngle; 
 
 public trialAngle(float thetaArrow,float thetaAngle)
 {
   mThetaArrow = thetaArrow;
   mThetaAngle = thetaAngle;
 }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Rotation" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
