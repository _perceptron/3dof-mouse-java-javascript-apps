import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class rotateAndTranslateCar extends PApplet {

/* @pjs preload="movableCar.png"; */
/* @pjs preload="carmod_small.png"; */
/* @pjs preload="rotate.png"; */


PImage movableCar;
PImage background;
PImage cursorMove;
PImage cursorRotate;

float bx;
float by;
int boxSize = 75;
boolean overBox = false;
boolean locked = false;
float xOffset = 0.0f; 
float yOffset = 0.0f; 
float movableCarX = 30;
float movableCarY = 250;
float carSizeX = 230;
float carSizeY = 130;

float angle = 0.0f;

public void setup() 
{
  size(640, 480);
  bx = width/2.0f;
  by = height/2.0f;
 // rectMode(RADIUS);  
 movableCar = loadImage("movableCar.png");
 background = loadImage("carmod_small.png");
 cursorRotate = loadImage("rotate.png");
}



public void draw() 
{ 
  background(242,241,236);
  // Test if the cursor is over the box 
  image(movableCar,(width/2)-(carSizeX/2),(height/2)-(carSizeY/2),carSizeX,carSizeY);
  pushMatrix();
  translate(movableCarX, movableCarY);
  rotate(0); 
  image(movableCar,0,0,carSizeX,carSizeY);
  if (isCursorOverCar()) {
      overBox = true; 
      image(cursorRotate,(carSizeX/2),(-24),24,24); 
      if(!locked) { 
        stroke(100);
        noFill();
        rect(0,0,carSizeX,carSizeY); 
      } 
      
      else{
       fill(150,50);
       stroke(0);
       rect(0,0,carSizeX,carSizeY); 
      }
  } else {
      stroke(255);
      noFill();
      rect(0,0,carSizeX,carSizeY); 
      overBox = false;
  } 
   
  if(isCursorOverRotateRegion()){
      image(cursorRotate,(0+carSizeX/2),(0-24),24,24); 
  }  
  
  popMatrix();  

    
}

public boolean isCursorOverCar(){
  if (mouseX > movableCarX && mouseX < movableCarX+carSizeX && 
        mouseY > movableCarY && mouseY < movableCarY+carSizeY){
        return true; 
        }
        
        return false;
}

public boolean isCursorOverRotateRegion(){
  if ((mouseX > (movableCarX+carSizeX/2-24))  && (mouseX < (movableCarX+carSizeX/2+48)) && 
        (mouseY > (movableCarY-48)) && mouseY < movableCarY+24){
        return true; 
        }
        
        return false;
}

public void mousePressed() {
  if(overBox) { 
      locked = true; 
      cursor(HAND);
  } else {
      locked = false;
  }
  xOffset = mouseX-movableCarX; 
  yOffset = mouseY-movableCarY; 

}

public void mouseDragged() {
  if(locked) {
      movableCarX = mouseX-xOffset; 
      movableCarY = mouseY-yOffset; 
  }
  if(isCursorOverRotateRegion()){
  angle = atan2(mouseY-(movableCarY), mouseX-(movableCarX+carSizeX/2));
  }
}

public void mouseReleased() {
  locked = false;
  cursor(ARROW);
}

public void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  println(e);
}

public void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0f;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
      float sx = x + cos(a) * radius2;
      float sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a+halfAngle) * radius1;
      sy = y + sin(a+halfAngle) * radius1;
      vertex(sx, sy);
  }
  endShape(CLOSE);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "rotateAndTranslateCar" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
