import processing.opengl.*;
import codeanticode.glgraphics.*;
//import processing.core.PGraphics3D;
import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.core.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.providers.*;


UnfoldingMap map;

void setup() {
    size(800, 600, GLConstants.GLGRAPHICS);
//    size(800, 600, P3D);
    map = new UnfoldingMap(this);
    MapUtils.createDefaultEventDispatcher(this, map);
}

void draw() {
    map.draw();
}
