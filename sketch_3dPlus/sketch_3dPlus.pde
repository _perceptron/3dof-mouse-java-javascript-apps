/* @pjs preload="movableCar.png"; */
/* @pjs preload="carmod_small.png"; */
/* @pjs preload="rotate.png"; */


PImage movableCar;
PImage background;
PImage cursorMove;
PImage cursorRotate;

float bx;
float by;
int boxSize = 75;
boolean overBox = false;
boolean locked = false;
float xOffset = 0.0; 
float yOffset = 0.0; 
float movableCarX = 30;
float movableCarY = 250;
float carSizeX = 230;
float carSizeY = 130;

float angle = 0.0;

void setup() 
{
  size(640, 480);
  bx = width/2.0;
  by = height/2.0;
 // rectMode(RADIUS);  
 movableCar = loadImage("movableCar.png");
 background = loadImage("carmod_small.png");
 cursorRotate = loadImage("rotate.png");
}



void draw() 
{ 
  background(242,241,236);
  // Test if the cursor is over the box 
  image(movableCar,350,50,carSizeX,carSizeY);
  pushMatrix();
  translate(movableCarX, movableCarY);
  rotate(angle); 
  image(movableCar,0,0,carSizeX,carSizeY);
  if (isCursorOverCar()) {
      overBox = true; 
      image(cursorRotate,(carSizeX/2),(-24),24,24); 
      if(!locked) { 
        stroke(100);
        noFill();
        rect(0,0,carSizeX,carSizeY); 
      } 
      
      else{
       fill(150,50);
       stroke(0);
       rect(0,0,carSizeX,carSizeY); 
      }
  } else {
      stroke(255);
      noFill();
      rect(0,0,carSizeX,carSizeY); 
      overBox = false;
  } 
   
  if(isCursorOverRotateRegion()){
      image(cursorRotate,(0+carSizeX/2),(0-24),24,24); 
  }  
  
  popMatrix();  

    
}

boolean isCursorOverCar(){
  if (mouseX > movableCarX && mouseX < movableCarX+carSizeX && 
        mouseY > movableCarY && mouseY < movableCarY+carSizeY){
        return true; 
        }
        
        return false;
}

boolean isCursorOverRotateRegion(){
  if ((mouseX > (movableCarX+carSizeX/2-24))  && (mouseX < (movableCarX+carSizeX/2+48)) && 
        (mouseY > (movableCarY-48)) && mouseY < movableCarY+24){
        return true; 
        }
        
        return false;
}

void mousePressed() {
  if(overBox) { 
      locked = true; 
      cursor(HAND);
  } else {
      locked = false;
  }
  xOffset = mouseX-movableCarX; 
  yOffset = mouseY-movableCarY; 

}

void mouseDragged() {
  if(locked) {
      movableCarX = mouseX-xOffset; 
      movableCarY = mouseY-yOffset; 
  }
  if(isCursorOverRotateRegion()){
  angle = atan2(mouseY-(movableCarY), mouseX-(movableCarX+carSizeX/2));
  }
}

void mouseReleased() {
  locked = false;
  cursor(ARROW);
}

void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  println(e);
}

void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
      float sx = x + cos(a) * radius2;
      float sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a+halfAngle) * radius1;
      sy = y + sin(a+halfAngle) * radius1;
      vertex(sx, sy);
  }
  endShape(CLOSE);
}
